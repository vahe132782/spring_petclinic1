FROM maven:3.8.7-openjdk-18 as build
WORKDIR /opt
ARG app_version
COPY . .    
RUN mvn versions:set -DnewVersion=1.0.${app_version} && mvn package -DskipTests


FROM maven:3.8.7-openjdk-18-slim
WORKDIR /opt
ARG app_version
COPY --from=build /opt/target/spring-petclinic-1.0.${app_version}.jar .
CMD java -jar /opt/spring-petclinic-1.0.${app_version}.jar
